﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using DevExpress.Mvvm;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.Native;
using DevExpress.Mvvm.POCO;
using Microsoft.Win32;
using Newtonsoft.Json;
using Teeatron.Tealogic.BaseClasses;

namespace Teeatron.ViewModels
{
    [POCOViewModel]
    public class TeatronVM
    {
#region Fields

        //public string path = $@"C:\Users\kille\GitLina\teaui\Teeatron\Teeatron\TeaLogic\BaseClasses\tea.json";
        public DrinkingSessionVM DrinkingSessionVM;
        public EvaluationVM EvaluationVM;
        public InfusionVM InfusionVM;
        public string path = $@"{Directory.GetCurrentDirectory()}\Tea.json";
        public TeaVM TeaDialogVM;

#endregion

#region Properties, Indexers

        protected IDialogService DialogService => this.GetService<IDialogService>();
        public Infusion SelectedInfusion { get; set; }
        public ObservableCollection<DataPoint> DataPoints { get; set; }
        public virtual DialogWindow CurrentDialogWindow { get; set; }
        public virtual ObservableCollection<DrinkingSession> DrinkingSessions { get; set; }
        public virtual ObservableCollection<Infusion> InfusionList { get; set; }
        public virtual DrinkingSession SelectedDrinkingSession { set; get; }
        public virtual Tea SelectedTea { set; get; }
        public virtual int SelectedTeaIndex { set; get; }
        public virtual int SelectedDrinkingSessionIndex { set; get; }
        public virtual ObservableCollection<Tea> Teas { get; set; }

#endregion

#region Constructors

        public TeatronVM()
        {
            DataPoints = new ObservableCollection<DataPoint>();
            Load();
        }

#endregion

#region Methods

        public void AddInfusion(DrinkingSession drinkingSession, int Time, int Points)
        {
            drinkingSession.Infusionlist.Add(new Infusion(Points, Time));
            Save();
        }

        public void AddTea(string Name, int Number, KindOfTea kindOfTea, double weight, double price)
        {
            if (Teas == null) Teas = new ObservableCollection<Tea>();
            Teas.Add(new Tea(Name, Number, kindOfTea, weight, price));
            Save();
        }

        public void EditDrinkingSession(
            DrinkingSession session,
            double weight,
            int milliliter,
            long Date,
            SessionKind sessionKind)
        {
            session.EditValues(weight, milliliter, Date, sessionKind);
            Save();
        }

        public void EditTea(Tea tea, string Name, int Number, KindOfTea kindOfTea, double weight, double price)
        {
            tea.Name = Name;
            tea.Number = Number;
            tea.KindOfTea = kindOfTea;
            tea.BruttoWeight = weight;
            tea.Price = price;
        }

        public void ImportDataBase()
        {
            var openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            openFileDialog.Filter = "Json (*.json)|*.json|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
            {
                Teas = JsonConvert.DeserializeObject<ObservableCollection<Tea>>(
                    File.ReadAllText(Path.GetFullPath(openFileDialog.FileName)));

                Save();
            }
        }

        public void OnInfusionListChanged()
        {
            Save();
        }

        public void RemoveDrinkingSession() { }

        public void RemoveInfusion(DrinkingSession drinkingSession)
        {
            SelectedTea.DrinkingSessions.Remove(drinkingSession);
            Save();
        }


        public void RemoveObject(object teaObject)
        {
            switch (teaObject)
            {
                //case Tea:
                //    break;
                //case DrinkingSession:
                //    break;
            }
        }

        public void RemoveTea()
        {
            Teas.Remove(SelectedTea);
            Save();
        }

        public void Reset(int temptea, int tempDrinkingSession)
        {
            if (temptea >= 0) SelectedTea = Teas[temptea];
            if (tempDrinkingSession >= 0 && Teas[temptea].DrinkingSessions.Count > 0)
                SelectedDrinkingSession = SelectedTea.DrinkingSessions[tempDrinkingSession];
        }

        public void Save()
        {
            File.WriteAllText(path, JsonConvert.SerializeObject(Teas, Formatting.Indented));
        }

        private void AddDrinkingSession(double weight, int milliliter, long Date, SessionKind sessionKind)
        {
            SelectedTea.DrinkingSessions.Add(new DrinkingSession(weight, milliliter, Date, sessionKind));
            Save();
        }


        private void Load()
        {
            if (!File.Exists(path)) Save();
            Teas = JsonConvert.DeserializeObject<ObservableCollection<Tea>>(File.ReadAllText(path));
            DataPoints.Clear();
            Teas.OrderByDescending(t => t.Evaluation)
                .ForEach(t => DataPoints.Add(new DataPoint(t.Name, t.Evaluation, t.Points)));
        }

        private void ReloadRoutine()
        {
            var tempDrinkingSession = SelectedDrinkingSessionIndex;
            var temptea = SelectedTeaIndex;
            Save();
            Load();
            Reset(temptea, tempDrinkingSession);
        }

#endregion

#region Open Dialogs

        public void ShowAddEvaluationInfussion(Infusion infusion)
        {
            if (EvaluationVM == null) EvaluationVM = new EvaluationVM();

            EvaluationVM.Evaluation = infusion.Evaluation;
            EvaluationVM.Comment = infusion.Comment;
            var registerCommand = new UICommand
            {
                Caption = "JA",
                IsCancel = false,
                IsDefault = true,
                Command = new DelegateCommand<CancelEventArgs>(
                    x => infusion.Evaluate(EvaluationVM.Evaluation, EvaluationVM.Comment),
                    x => !string.IsNullOrEmpty(EvaluationVM.Evaluation.ToString()))
            };

            var cancelCommand = new UICommand
            {
                Id = MessageBoxResult.Cancel, Caption = "Cancel", IsCancel = true, IsDefault = false
            };

            var result = DialogService.ShowDialog(
                new List<UICommand> {registerCommand, cancelCommand},
                "Registration Dialog",
                EvaluationVM);

            if (result == registerCommand) ReloadRoutine();
        }

        public void ShowAddEvaluationSession(DrinkingSession session)
        {
            if (EvaluationVM == null) EvaluationVM = new EvaluationVM();

            EvaluationVM.Evaluation = session.SessionImpression;
            EvaluationVM.Comment = session.Comment;
            var registerCommand = new UICommand
            {
                Caption = "JA",
                IsCancel = false,
                IsDefault = true,
                Command = new DelegateCommand<CancelEventArgs>(
                    x => session.Evaluate(EvaluationVM.Evaluation, EvaluationVM.Comment),
                    x => !string.IsNullOrEmpty(EvaluationVM.Evaluation.ToString()))
            };

            var cancelCommand = new UICommand
            {
                Id = MessageBoxResult.Cancel, Caption = "Cancel", IsCancel = true, IsDefault = false
            };


            var result = DialogService.ShowDialog(
                new List<UICommand> {registerCommand, cancelCommand},
                $"Evaluate Session {new DateTime(session.Datum).ToString("dd.MM.")}",
                EvaluationVM);

            if (result == registerCommand) ReloadRoutine();
        }

        public void ShowAddInfusion(DrinkingSession drinkingSession)
        {
            if (InfusionVM == null) InfusionVM = new InfusionVM();

            var registerCommand = new UICommand
            {
                Caption = "Confirm",
                IsCancel = false,
                IsDefault = true,
                Command = new DelegateCommand<CancelEventArgs>(
                    x => AddInfusion(drinkingSession, InfusionVM.Time, InfusionVM.Temperature),
                    x => !string.IsNullOrEmpty(InfusionVM.Evaluation.ToString()))
            };

            var cancelCommand = new UICommand
            {
                Id = MessageBoxResult.Cancel, Caption = "Cancel", IsCancel = true, IsDefault = false
            };

            var result = DialogService.ShowDialog(
                new List<UICommand> {registerCommand, cancelCommand},
                "Registration Dialog",
                InfusionVM);

            if (result == registerCommand) ReloadRoutine();
        }

        public void ShowAddSession()
        {
            if (DrinkingSessionVM == null) DrinkingSessionVM = new DrinkingSessionVM();

            var registerCommand = new UICommand
            {
                Caption = "Confirm",
                IsCancel = false,
                IsDefault = true,
                Command = new DelegateCommand<CancelEventArgs>(
                    x => AddDrinkingSession(
                        DrinkingSessionVM.Weight,
                        DrinkingSessionVM.Milliliter,
                        DrinkingSessionVM.Date.Ticks,
                        DrinkingSessionVM.SessionKind),
                    x => !string.IsNullOrEmpty(DrinkingSessionVM.Date.ToString()))
            };

            var cancelCommand = new UICommand
            {
                Id = MessageBoxResult.Cancel, Caption = "Cancel", IsCancel = true, IsDefault = false
            };

            var result = DialogService.ShowDialog(
                new List<UICommand> {registerCommand, cancelCommand},
                "Registration Dialog",
                DrinkingSessionVM);

            if (result == registerCommand) ReloadRoutine();
        }

        public void ShowAddTea()
        {
            if (TeaDialogVM == null) TeaDialogVM = new TeaVM();

            var registerCommand = new UICommand
            {
                Caption = "Confirm",
                IsCancel = false,
                IsDefault = true,
                Command = new DelegateCommand<CancelEventArgs>(
                    x => AddTea(
                        TeaDialogVM.Name,
                        TeaDialogVM.Number,
                        TeaDialogVM.KindOfTea,
                        TeaDialogVM.Weight,
                        TeaDialogVM.Price),
                    x => !string.IsNullOrEmpty(TeaDialogVM.Name))
            };

            var cancelCommand = new UICommand
            {
                Id = MessageBoxResult.Cancel, Caption = "Cancel", IsCancel = true, IsDefault = false
            };

            var result = DialogService.ShowDialog(
                new List<UICommand> {registerCommand, cancelCommand},
                "Registration Dialog",
                TeaDialogVM);

            if (result == registerCommand) ReloadRoutine();
        }

        public void ShowEditSession(DrinkingSession session)
        {
            if (DrinkingSessionVM == null) DrinkingSessionVM = new DrinkingSessionVM();
            DrinkingSessionVM.Weight = session.SessionTeaWeight;
            DrinkingSessionVM.Milliliter = session.SessionMilliliters;
            DrinkingSessionVM.SessionKind = session.SessionKind;

            var registerCommand = new UICommand
            {
                Caption = "Confirm",
                IsCancel = false,
                IsDefault = true,
                Command = new DelegateCommand<CancelEventArgs>(
                    x => EditDrinkingSession(
                        session,
                        DrinkingSessionVM.Weight,
                        DrinkingSessionVM.Milliliter,
                        DrinkingSessionVM.Date.Ticks,
                        DrinkingSessionVM.SessionKind),
                    x => !string.IsNullOrEmpty(DrinkingSessionVM.Date.ToString()))
            };

            var cancelCommand = new UICommand
            {
                Id = MessageBoxResult.Cancel, Caption = "Cancel", IsCancel = true, IsDefault = false
            };

            var result = DialogService.ShowDialog(
                new List<UICommand> {registerCommand, cancelCommand},
                "Registration Dialog",
                DrinkingSessionVM);

            if (result == registerCommand) ReloadRoutine();
        }

        public void ShowEditTea(Tea tea)
        {
            if (TeaDialogVM == null) TeaDialogVM = new TeaVM();
            TeaDialogVM.Number = tea.Number;
            TeaDialogVM.KindOfTea = tea.KindOfTea;
            TeaDialogVM.Name = tea.Name;
            TeaDialogVM.Weight = tea.BruttoWeight;
            TeaDialogVM.Price = tea.Price;
            var registerCommand = new UICommand
            {
                Caption = "Bestätigen",
                IsCancel = false,
                IsDefault = true,
                Command = new DelegateCommand<CancelEventArgs>(
                    x => EditTea(
                        tea,
                        TeaDialogVM.Name,
                        TeaDialogVM.Number,
                        TeaDialogVM.KindOfTea,
                        TeaDialogVM.Weight,
                        TeaDialogVM.Price),
                    x => !string.IsNullOrEmpty(TeaDialogVM.Name))
            };

            var cancelCommand = new UICommand
            {
                Id = MessageBoxResult.Cancel, Caption = "Abbrechen", IsCancel = true, IsDefault = false
            };

            var result = DialogService.ShowDialog(
                new List<UICommand> {registerCommand, cancelCommand},
                "Tee Bearbeiten",
                TeaDialogVM);

            if (result == registerCommand) ReloadRoutine();
        }

#endregion
    }

    public class DataPoint
    {
#region Properties, Indexers

        public string Argument { get; set; }
        public double Value1 { get; set; }
        public double Value2 { get; set; }

#endregion

#region Constructors

        public DataPoint(string argument, double value1, double value2)
        {
            Argument = argument;
            Value1 = value1;
            Value2 = value2;
        }

#endregion
    }
}