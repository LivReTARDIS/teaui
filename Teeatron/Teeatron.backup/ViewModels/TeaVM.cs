﻿using System.Collections.ObjectModel;
using DevExpress.Mvvm.DataAnnotations;
using DevExpress.Mvvm.Native;
using Teeatron.Tealogic.BaseClasses;


namespace Teeatron.ViewModels
{
    [POCOViewModel]
    public partial class TeaVM : DialogViewModel
    {
        public virtual string Name { get; set; }
        public virtual int Number { get; set; }
        public virtual double Price { get; set; }
        public virtual double Weight { get; set; }
        public virtual KindOfTea KindOfTea { get; set; }


        public TeaVM()
        {
            DialogWindow = DialogWindow.Tea;
        }
    }
}