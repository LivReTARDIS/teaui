﻿using System;
using DevExpress.Mvvm.DataAnnotations;
using Teeatron.Tealogic.BaseClasses;

namespace Teeatron.ViewModels
{
    [POCOViewModel]
    public class DrinkingSessionVM :DialogViewModel
    {
        public virtual DateTime Date { get; set; }
        public virtual SessionKind SessionKind { get; set; }
        public virtual double Weight { get; set; }
        public virtual int  Milliliter { get; set; }
        public virtual int Evaluation { get; set; }

        public DrinkingSessionVM()
        {
            DialogWindow = DialogWindow.DrinkingSession;
        }
    }
}