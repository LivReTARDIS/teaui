﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Teeatron.ViewModels;

namespace Teeatron.Converter
{
    public class DialogWindowSelector :DataTemplateSelector
    {
  
        public DataTemplate TemplateTea { get; set; }
        public DataTemplate TemplateSession { get; set; }
        public DataTemplate TemplateInfuson { get; set; }
        public DataTemplate TemplateEvaluation { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            DialogViewModel viewModel = item as DialogViewModel;
            if (viewModel != null)
            {
                switch (viewModel.DialogWindow)
                {
                    case DialogWindow.Tea:
                        return TemplateTea;
                    case DialogWindow.DrinkingSession:
                        return TemplateSession;
                    case DialogWindow.Infusion:
                        return TemplateInfuson;
                    case DialogWindow.Evaluation:
                        return TemplateEvaluation;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            return base.SelectTemplate(item, container);
        }
    }
}
