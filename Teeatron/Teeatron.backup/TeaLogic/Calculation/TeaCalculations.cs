﻿using System.Linq;
using Teeatron.Tealogic.BaseClasses;


namespace Teeatron.TeaLogic.Calculation
{
    public static class TeaCalculations
    {
        #region Public Methods
        public static int EvaluateTeasPoints(this Tea tea)
        {
            if (tea.DrinkingSessions.Count == 0) return 0;
            return tea.DrinkingSessions.Sum(d => d.Infusionlist.Sum(i=>i.Evaluation));
        }
        public static double EvaluateTheSession(this DrinkingSession drinkinSession)
        {
            if (drinkinSession.Infusionlist.All(i => i.Evaluation == 0) || drinkinSession.SessionImpression ==0) return 0;
            if (drinkinSession.Infusionlist == null && drinkinSession.Infusionlist.Count== 0) return 0;
            return System.Math.Round((double)drinkinSession.Infusionlist
                       .Where(i =>i.Evaluation != 0)
                       .Sum(i => i.Evaluation) 
                   / drinkinSession.Infusionlist.Where(i => i.Evaluation != 0).ToList().Count * 2 / 3 
                   +
                  ((double)drinkinSession.SessionImpression* 1 / 3),2);
        }

        public static double EvaluateTeasEvaluation(this Tea tea)
        {

            return System.Math.Round(tea.DrinkingSessions
                .Where(d => d.SessionEvaluation!=0)
                .Sum(d => d.SessionEvaluation) /
                                     tea.DrinkingSessions
                                         .Where(d => d.SessionEvaluation != 0)
                                         .ToList().Count, 2);
        }
        #endregion
    }
}