﻿using System.Collections.ObjectModel;
using System.Linq;
using Newtonsoft.Json;
using Teeatron.TeaLogic.BaseClasses;
using Teeatron.TeaLogic.Calculation;


namespace Teeatron.Tealogic.BaseClasses
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Tea
    {
#region Properies
        [JsonProperty] public ObservableCollection<DrinkingSession> DrinkingSessions { get; set; }
        public double Evaluation
        {
            get => this.EvaluateTeasEvaluation();
    
        }
        public int Points
        {
            get => this.EvaluateTeasPoints();
        }

        [JsonProperty] public KindOfTea KindOfTea { get; set; }
        [JsonProperty] public string Name { get; set; }
        [JsonProperty] public int Number { get; set; }
        [JsonProperty] public double BruttoWeight { get; set; }

        [JsonProperty]
        public double NettoWeight
        {
            get
            {
                return BruttoWeight - DrinkingSessions.Sum(d => d.SessionTeaWeight);
            }
        }

        [JsonProperty] public double Price { get; set; }
        
       
        [JsonProperty] public StandartValues TeaSpecificStandarts { get; set; }
#endregion


#region Constructor
        public Tea(string name, int number, KindOfTea kindOfTea,double BruttoWeight=-1,double price=-1)
        {
            Name = name;
            Number = number;
            KindOfTea = kindOfTea;
            DrinkingSessions = new ObservableCollection<DrinkingSession>();
            this.BruttoWeight = BruttoWeight;
            Price = price;
        }
#endregion


#region Public Methods
        // price and Total weigth might be added Laeter 
        // favourite sternchen 

        // Gast tees 
        public override string ToString()
        {
            return $"{Number} {Name}";
        }
#endregion
    }
    public enum KindOfTea
    {
        Grün,
        Oolong,
        Aroma,
        Yasmin,
        Schwarz,
        Weiß
    }
}


