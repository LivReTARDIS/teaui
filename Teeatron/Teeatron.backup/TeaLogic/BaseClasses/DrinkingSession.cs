﻿using System;
using System.Collections.ObjectModel;
using Teeatron.TeaLogic.Calculation;

namespace Teeatron.Tealogic.BaseClasses
{
    public class DrinkingSession
    {
        #region Fields


        #endregion


        #region Constructor

        public DrinkingSession(double sessionTeaWeight, int sessionMilliliters, long datum, SessionKind sessionKind)
        {
            SessionTeaWeight = sessionTeaWeight;
            SessionMilliliters = sessionMilliliters;
            Datum = datum;
            SessionKind = sessionKind;
            Infusionlist = new ObservableCollection<Infusion>();
        }

        #endregion


        #region Public Methods

        public void AddInfusion()
        {
            Infusionlist.Add(new Infusion(5, 20));
        }

        public void Evaluate(int val,string comment)
        {
            if(comment != "")
            Comment = comment;
            SessionImpression = val;

        }

        #endregion


        #region Properies

        public string Comment { get; set; }
        public long Datum { get; set; }

        public double SessionEvaluation
        {
            get => this.EvaluateTheSession();
        }
        public int SessionImpression { get; set; }
        public ObservableCollection<Infusion> Infusionlist { get; set; }
        public int SessionMilliliters { get; set; }
        public double SessionTeaWeight { get; set; }
        public SessionKind SessionKind { get; set; }
        #endregion

        public void EditValues(double sessionTeaWeight, int sessionMilliliters, long datum, SessionKind sessionKind)
        {
            SessionTeaWeight = sessionTeaWeight;
            SessionMilliliters = sessionMilliliters;
            Datum = datum;
            SessionKind = sessionKind;
        }
    }


    public enum SessionKind
    {
        HotBrew,
        ColdBrew
    }
}