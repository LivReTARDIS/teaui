﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using Teeatron.Tealogic.BaseClasses;


namespace Teeatron.ConverterAndSelector
{
    class SessionKindToColorConverter : IValueConverter
    {
        
        //Frage an Tom :
        // Wie kann ich auf Recourcen zugreifen hier? BZW kann ich das im XAML definien?
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
       
            if (value is SessionKind sk)
            {
                switch (sk)
                {
                    case SessionKind.HotBrew:
                        return new SolidColorBrush(Colors.DarkRed);
                    case SessionKind.ColdBrew:
                        return new SolidColorBrush(Colors.DarkBlue);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return new SolidColorBrush(Colors.Black);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
