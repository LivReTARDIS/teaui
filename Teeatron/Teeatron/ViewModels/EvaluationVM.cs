﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm.DataAnnotations;

namespace Teeatron.ViewModels
{
    [POCOViewModel]
  public   class EvaluationVM : DialogViewModel
    {
        public virtual string Comment { get; set; }
        public virtual int Evaluation { get; set; }

        public EvaluationVM()
        {
            DialogWindow = DialogWindow.Evaluation;
        }
    }
}
