﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm.DataAnnotations;

namespace Teeatron.ViewModels
{
    [POCOViewModel]
    public class InfusionVM: DialogViewModel
    {
        public virtual int  Time { get; set; }
        public virtual int  Temperature { get; set; }
        public virtual int  Evaluation { get; set; }
        public virtual string  Comment { get; set; }

        public InfusionVM()
        {
            DialogWindow = DialogWindow.Infusion;
        }
    }
}
