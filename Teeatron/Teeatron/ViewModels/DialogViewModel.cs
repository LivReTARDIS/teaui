﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.XtraPrinting.Native.Lines;

namespace Teeatron.ViewModels
{
    public abstract class DialogViewModel
    {
        public virtual DialogWindow  DialogWindow { get; set; }
    }

    public enum DialogWindow
    {
        Tea,
        DrinkingSession,
        Infusion,
        Evaluation
    }
}
