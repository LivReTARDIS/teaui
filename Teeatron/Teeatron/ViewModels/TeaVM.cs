﻿using DevExpress.Mvvm.DataAnnotations;
using Teeatron.Tealogic.BaseClasses;

namespace Teeatron.ViewModels
{
    [POCOViewModel]
    public class TeaVM : DialogViewModel
    {
#region Properties, Indexers

        public virtual string Name { get; set; }
        public virtual string ShortName { get; set; }
        public virtual int Number { get; set; }
        public virtual double Price { get; set; }
        public virtual double Weight { get; set; }
        public virtual KindOfTea KindOfTea { get; set; }

#endregion

#region Constructors

        public TeaVM()
        {
            DialogWindow = DialogWindow.Tea;
        }

#endregion
    }
}