﻿namespace Teeatron.Tealogic.BaseClasses
{
    public class Infusion
    {
#region Properies
        public int Evaluation { get; set; }
        public int InfusionTime { get; set; }
        public int InfusionTemperature { get; set; }
        public string Comment { get; set; }

#endregion


#region Constructor
        public Infusion(int temp, int infusionTime)
        {
            InfusionTemperature = temp;
            InfusionTime = infusionTime;
        }
#endregion


public void Evaluate(int ev, string com)
{
    Evaluation = ev;
    Comment = com;
}
#region Public Methods
        public override string ToString()
        {
            return $" Points: {Evaluation} Time : {InfusionTime}";
        }
#endregion
    }
}