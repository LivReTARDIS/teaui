﻿namespace Teeatron.TeaLogic.BaseClasses
{
    public class StandartValues
    {
        #region Properies
        public int StandartInfusionTeaWeight { get; set; }
        public int[] StandartInfusionTemperatures { get; set; }
        public int[] StandartInfusionTimes { get; set; }
        public int StandartMilliters { get; set; }
        #endregion


        #region Constructor
        public StandartValues(int[] standartInfusionTimes, int[] standartInfusionTemperatures,
            int standartInfusionTeaWeight, int standartMilliters)
        {
            StandartInfusionTimes = standartInfusionTimes;
            StandartInfusionTemperatures = standartInfusionTemperatures;
            StandartInfusionTeaWeight = standartInfusionTeaWeight;
            StandartMilliters = standartMilliters;
        }
        #endregion
    }
}