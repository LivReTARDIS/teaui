﻿using DevExpress.Xpf.Core;

namespace Teeatron
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ThemedWindow
    {
#region Constructors

#region Constructor

        public MainWindow()
        {
            InitializeComponent();
        }

#endregion

#endregion
    }
}